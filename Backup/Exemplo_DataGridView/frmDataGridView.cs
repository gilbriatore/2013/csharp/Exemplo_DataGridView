﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Exemplo_DataGridView
{
    public partial class frmDataGridView : Form
    {
        int cod;
        public frmDataGridView()
        {
            InitializeComponent();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            float salario;
            if (txtNome.Text != "" && txtSalario.Text != "")
            {
                salario = float.Parse(txtSalario.Text);
                grdResultados.Rows.Add(cod, txtNome.Text, salario.ToString("N2"));
                cod++;
                txtNome.Focus();
            }
            else
            {
                MessageBox.Show("Preencha todos os campos", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void frmDataGridView_Load(object sender, EventArgs e)
        {
            cod = 1;
        }

        private void btnPrimeira_Click(object sender, EventArgs e)
        {
            int i;
            try
            {
                for (i = 0; i < grdResultados.ColumnCount; i++)
                {
                    MessageBox.Show(grdResultados.Rows[0].Cells[i].Value.ToString());
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("Erro ao selecionar linha" , "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSelecionada_Click(object sender, EventArgs e)
        {
            int i;
            try
            {
                for (i = 0; i < grdResultados.ColumnCount; i++)
                {
                    MessageBox.Show(grdResultados.CurrentRow.Cells[i].Value.ToString());
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("Erro ao selecionar linha", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i;
            for (i = 0; i < grdResultados.Rows.Count - 1; i++)
            {
                MessageBox.Show(grdResultados.Rows[i].Cells[1].Value.ToString());
            }
        }
    }
}
